FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

RUN apt-get update && apt-get install -y openjdk-21-jdk-headless && rm -rf /var/cache/apt /var/lib/apt/lists

# https://files.minecraftforge.net/net/minecraftforge/forge/
ENV MC_VERSION=1.21.1
ENV FORGE_VERSION=52.0.9

RUN curl -L https://maven.minecraftforge.net/net/minecraftforge/forge/${MC_VERSION}-${FORGE_VERSION}/forge-${MC_VERSION}-${FORGE_VERSION}-installer.jar -o forge-installer.jar
RUN java -jar /app/code/forge-installer.jar --installServer /app/code/forge && \
    rm /app/code/forge-installer.jar /app/code/forge-installer.jar.log

COPY shared/frontend /app/code/frontend
COPY shared/backend /app/code/backend
COPY shared/index.js shared/package.json shared/package-lock.json start.sh server.properties.template /app/code/

RUN npm install

CMD [ "/app/code/start.sh" ]
