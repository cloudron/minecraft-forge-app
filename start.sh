#!/bin/bash

set -eu

echo "=> Ensure directories"
mkdir -p /app/data/
rm -rf /app/data/libraries && ln -sf /app/code/forge/libraries /app/data/libraries

echo "=> Accept EULA"
echo "eula=true" > /app/data/eula.txt

if [[ ! -f /app/data/server.properties ]]; then
    echo "=> Copy initial server.properties"
    cp /app/code/server.properties.template /app/data/server.properties
fi

echo "=> Update server port"
sed -e "s/server-port.*/server-port=${SERVER_PORT}/" -i /app/data/server.properties
echo "=> Update query port"
sed -e "s/query.port.*/query.port=${SERVER_PORT}/" -i /app/data/server.properties

if [[ -z "${RCON_PORT:-}" ]]; then
    echo "=> Disable rcon port"
    sed -e "s/rcon.port.*/rcon.port=/" -i /app/data/server.properties
    sed -e "s/enable-rcon.*/enable-rcon=false/" -i /app/data/server.properties
else
    echo "=> Update rcon port"
    sed -e "s/rcon.port.*/rcon.port=${RCON_PORT}/" -i /app/data/server.properties
    sed -e "s/enable-rcon.*/enable-rcon=true/" -i /app/data/server.properties
fi

echo "=> Ensure permissions"
chown -R cloudron:cloudron /app/data

echo "=> Starting management server"
export MC_FLAVOR="forge"
exec /usr/local/bin/gosu cloudron:cloudron node /app/code/index.js
