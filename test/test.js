#!/usr/bin/env node

/* jshint esversion: 8 */
/* global describe */
/* global before */
/* global after */
/* global it */
/* global xit */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME, PASSWORD vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const LOCATION = 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 30000;
    const MANIFEST = require('../CloudronManifest.json');

    let app, browser, flavor;
    var athenticated_by_oidc = false;

    before(function () {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1900, height: 1024 })).build();
        if (MANIFEST.id === 'net.minecraft.bedrock.cloudronapp') {
            flavor = 'bedrock';
        } else if (MANIFEST.id === 'net.minecraftforge.cloudronapp') {
            flavor = 'forge';
        } else {
            flavor = 'java';
        }
        console.log(`Running tests for minecraft flavor ${flavor}`);
    });

    after(function () {
        browser.quit();
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function loginOIDC(username, password) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}`);
        await browser.sleep(4000);

        await browser.findElement(By.xpath('//button[text()="Login"]')).click();
        await browser.sleep(4000);

        if (!athenticated_by_oidc) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.sleep(2000);
            await browser.findElement(By.id('loginSubmitButton')).click();
            await browser.sleep(2000);

            athenticated_by_oidc = true;
        }

        await waitForElement(By.xpath('//p[contains(text(), "Server Status")]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);
        await browser.sleep(4000);

        await browser.findElement(By.xpath('//a[contains(text(), "Logout")]')).click();

        await waitForElement(By.xpath('//button[text()="Login"]'));
    }

    async function startServer() {
        await browser.get(`https://${app.fqdn}`);
        await browser.sleep(4000);

        await browser.findElement(By.xpath('//button[contains(text(), "Start")]')).click();
        await browser.sleep(10000);

        await waitForElement(By.xpath('//span[contains(text(), "Running")]'));
    }

    async function stopServer() {
        await browser.get(`https://${app.fqdn}`);
        await browser.sleep(4000);

        await browser.findElement(By.xpath('//button[contains(text(), "Stop")]')).click();
        await browser.sleep(10000);

        await waitForElement(By.xpath('//span[contains(text(), "Stopped")]'));
    }

    async function checkLogs() {
        await browser.get(`https://${app.fqdn}`);
        await browser.sleep(2000);

        await waitForElement(By.xpath('//div[@class="logstream"]/p'));
        if (flavor === 'forge') {
            await waitForElement(By.xpath('//p[contains(text(), "Preparing spawn area")]'));
        } else if (flavor === 'java') {
            await waitForElement(By.xpath('//p[contains(text(), "Preparing spawn area")]'));
        } else if (flavor === 'bedrock') {
            await waitForElement(By.xpath('//p[contains(text(), "Opening level")]'));
        }
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login OIDC', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('check logs', checkLogs);
    it('can stop server', stopServer);
    it('can start server', startServer);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can login OIDC', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('check logs', checkLogs);
    it('can stop server', stopServer);
    it('can start server', startServer);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw --app ' + app.id));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });


    it('can get app information', getAppInfo);
    it('can login OIDC', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('check logs', checkLogs);
    it('can stop server', stopServer);
    it('can start server', startServer);
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can login OIDC', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('check logs', checkLogs);
    it('can stop server', stopServer);
    it('can start server', startServer);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('install app', function () { execSync('cloudron install --appstore-id ' + MANIFEST.id + ' --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login OIDC', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('check logs', checkLogs);
    it('can stop server', stopServer);
    it('can start server', startServer);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login OIDC', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('check logs', checkLogs);
    it('can stop server', stopServer);
    it('can start server', startServer);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
